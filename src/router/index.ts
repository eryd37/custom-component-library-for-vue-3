import { createRouter, createWebHistory } from "vue-router";
import ShowcaseView from "@/views/showcase/ShowcaseView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [{ path: "/", name: "home", component: ShowcaseView }],
});

export default router;
