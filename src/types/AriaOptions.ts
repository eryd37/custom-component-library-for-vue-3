export interface AriaOptions {
  label?: string;
  role?: string;
  orientation?: "horizontal" | "vertical" | undefined;
  labelledBy?: string;
  controls?: string;
  expanded?: boolean;
}
