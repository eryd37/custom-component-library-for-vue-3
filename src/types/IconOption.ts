export interface IconOption {
  name: string;
  size?: "tiny" | "small" | "normal" | "large";
  color?: string;
}
