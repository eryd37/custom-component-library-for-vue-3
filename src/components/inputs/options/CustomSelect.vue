<script setup lang="ts">
import Icon from "@/components/decoration/Icon.vue";
import LabelText from "@/components/inputs/LabelText.vue";
import Popover from "@/components/modals/Popover.vue";
import type { Option } from "@/types/Option";
import { ref, withDefaults } from "vue";

const props = withDefaults(
  defineProps<{
    modelValue?: Option;
    prompt?: string;
    options: Option[];
    label: string;
  }>(),
  {
    modelValue: undefined,
    prompt: "Select",
  }
);

const open = ref(false);
const popoverAnchor = ref<HTMLElement>();

function capitalize(word: string) {
  return word.charAt(0).toUpperCase() + word.slice(1);
}

function captureKeys(event: KeyboardEvent) {
  if (
    event.key === "ArrowUp" ||
    event.key === "ArrowLeft" ||
    event.key === "ArrowDown" ||
    event.key === "ArrowRight" ||
    event.key === " " ||
    event.key === "PageUp" ||
    event.key === "PageDown" ||
    event.key === "Home" ||
    event.key === "End" ||
    event.key === "Enter" ||
    (open.value && event.key === "Tab")
  ) {
    event.preventDefault();
  }
  if (event.key === "ArrowUp") {
    selectPrevious();
    highlightPrevious();
  } else if (event.key === "ArrowDown") {
    selectNext();
    highlightNext();
  } else if (event.key === "ArrowLeft") {
    selectPrevious();
  } else if (event.key === "ArrowRight") {
    selectNext();
  } else if (event.key === "PageUp" || event.key === "Home") {
    selectFirst();
    highlightFirst();
  } else if (event.key === "PageDown" || event.key === "End") {
    selectLast();
    highlightLast();
  }
}

function setupKeyCapture() {
  window.addEventListener("keydown", captureKeys);
}

function teardownKeyCapture() {
  window.removeEventListener("keydown", captureKeys);
}

const emit = defineEmits<{
  (event: "update:modelValue", value: Option): void;
}>();

function selectOption(option: Option) {
  emit("update:modelValue", option);
  open.value = false;
}

function selectFirst() {
  if (!open.value) {
    if (props.options.length) {
      selectOption(props.options[0]);
    }
  }
}

function selectLast() {
  if (!open.value) {
    if (props.options.length) {
      selectOption(props.options[props.options.length - 1]);
    }
  }
}

function selectPrevious() {
  if (!open.value) {
    const currentIndex =
      props.options.findIndex((value) => value.key === props.modelValue?.key) ??
      0;
    if (props.options.length) {
      if (currentIndex > 0) {
        selectOption(props.options[currentIndex - 1]);
      }
    }
  }
}

function selectNext() {
  if (!open.value) {
    const currentIndex =
      props.options.findIndex((value) => value.key === props.modelValue?.key) ??
      0;
    if (props.options.length) {
      if (currentIndex < props.options.length - 1) {
        selectOption(props.options[currentIndex + 1]);
      }
    }
  }
}

const highlightedOption = ref<Option>();

function handleOpen() {
  open.value = true;
  highlightedOption.value = props.modelValue;
}

function toggleOpen() {
  if (!open.value) {
    handleOpen();
  } else {
    open.value = false;
  }
}

function openMenu() {
  if (!open.value) {
    handleOpen();
  }
}

const optionsRef = ref<HTMLElement>();
const maxVisibleOptions = 5;

function scrollToOption(index: number) {
  if (optionsRef.value) {
    const optionDiv = optionsRef.value.children.item(
      index
    ) as HTMLDivElement | null;
    if (optionDiv) {
      const scrollY = optionsRef.value.scrollTop;
      const optionHeight = optionDiv.getBoundingClientRect().height;
      const topVisibleIndex = Math.floor(scrollY / optionHeight);
      const bottomVisibleIndex = topVisibleIndex + maxVisibleOptions - 1;
      if (index > bottomVisibleIndex) {
        if (index - bottomVisibleIndex > 1) {
          const newYPosition = optionHeight * (props.options.length - 1);
          optionsRef.value.scrollTop = newYPosition;
        } else {
          const newYPosition = optionHeight * (topVisibleIndex + 1);
          optionsRef.value.scrollTop = newYPosition;
        }
      } else if (index < topVisibleIndex) {
        const newYPosition = optionHeight * index;
        optionsRef.value.scrollTop = newYPosition;
      }
    }
  }
}

function highlightOption(index: number) {
  highlightedOption.value = props.options.at(index);
  scrollToOption(index);
}

function highlightFirst() {
  highlightOption(0);
}

function highlightLast() {
  highlightOption(props.options.length - 1);
}

function highlightNext() {
  const currentIndex = props.options.findIndex(
    (value) => value.key === highlightedOption.value?.key
  );
  if (currentIndex !== undefined) {
    const nextIndex = (currentIndex + 1) % props.options.length;
    highlightOption(nextIndex);
  } else {
    highlightOption(0);
  }
}

function highlightPrevious() {
  const currentIndex = props.options.findIndex(
    (value) => value.key === highlightedOption.value?.key
  );
  if (currentIndex !== undefined && currentIndex !== -1) {
    if (currentIndex === 0) {
      const nextIndex = props.options.length - 1;
      highlightOption(nextIndex);
    } else {
      highlightOption(currentIndex - 1);
    }
  } else {
    highlightOption(props.options.length - 1);
  }
}

function selectHighlightedAndClose() {
  if (open.value) {
    if (highlightedOption.value) {
      selectOption(highlightedOption.value);
    }
    open.value = false;
  }
}

function handleHoverOption(option: Option) {
  highlightedOption.value = option;
}

function handleLetterPress(event: KeyboardEvent) {
  const letter = event.key;
  if (highlightedOption.value?.name.toLocaleLowerCase().startsWith(letter)) {
    const matchingOptions = props.options.filter((value) =>
      value.name.toLocaleLowerCase().startsWith(letter)
    );
    const currentIndex = matchingOptions.findIndex(
      (value) => value.key === highlightedOption.value?.key
    );
    let matchingOption: Option | undefined;
    if (currentIndex === matchingOptions.length - 1) {
      matchingOption = matchingOptions.at(0);
    } else {
      matchingOption = matchingOptions.at(currentIndex + 1);
    }
    highlightedOption.value = matchingOption;
    const matchingOptionIndex = props.options.findIndex(
      (value) => value.key === matchingOption?.key
    );
    scrollToOption(matchingOptionIndex);
  } else {
    const matchingOption = props.options.find((value) =>
      value.name.toLocaleLowerCase().startsWith(letter)
    );
    if (matchingOption) {
      highlightedOption.value = matchingOption;
    }
  }
}
</script>

<template>
  <section class="custom-select">
    <label-text
      :id="`${label}-select-label`"
      :label-for="`${label}-select`"
      :label="capitalize(label)"
    />
    <button
      :id="`${label}-select`"
      ref="popoverAnchor"
      class="custom-select-field"
      tabindex="0"
      role="combobox"
      :aria-controls="`${label}-select-options`"
      :aria-expanded="open"
      aria-haspopup="listbox"
      :aria-labelledby="`${label}-select-label`"
      :aria-describedby="`${label}-select-info`"
      :aria-valuetext="modelValue?.name"
      :aria-owns="`${label}-select-options`"
      :aria-activedescendant="modelValue?.key"
      @click="toggleOpen"
      @keyup.tab="selectHighlightedAndClose"
      @keyup.enter="selectHighlightedAndClose"
      @keydown.space="openMenu"
      @keydown.esc="open = false"
      @keypress="handleLetterPress"
      @focus="setupKeyCapture"
      @blur="teardownKeyCapture"
    >
      <span aria-hidden="true">{{ modelValue?.name ?? prompt }}</span>
      <div class="icon-container" aria-hidden="true">
        <Icon name="chevron-down" />
      </div>
    </button>
    <span :id="`${label}-select-info`" class="visually-hidden">
      Bruk piltaster for å endre verdi eller space for å åpne meny
    </span>
    <Popover
      :open="open"
      :anchor="popoverAnchor"
      close-on-click-away
      :offset-top="1"
      :aria="{
        role: 'none',
      }"
      @close="open = false"
    >
      <ul
        :id="`${label}-select-options`"
        ref="optionsRef"
        class="options"
        tabindex="-1"
        aria-multiselectable="false"
        role="listbox"
      >
        <li
          v-for="option in options"
          :id="option.key"
          :key="option.key"
          class="option"
          :class="{ highlighted: option.key === highlightedOption?.key }"
          :aria-selected="option.key === modelValue?.key"
          role="option"
          tabindex="-1"
          @mouseenter="() => handleHoverOption(option)"
          @focus="() => {}"
          @click="() => selectOption(option)"
          @keyup.enter="() => selectOption(option)"
        >
          <label :id="`${option.key}-label`" :for="option.key">
            <span>{{ option.name }}</span>
          </label>
        </li>
      </ul>
    </Popover>
  </section>
</template>

<style scoped lang="scss">
.custom-select {
  display: flex;
  flex-flow: column nowrap;
  --width: 17rem;
  width: var(--width);
  --height: 3rem;
}

.custom-select-field {
  border: 1px solid var(--gray200);
  background-color: var(--gray50);
  justify-content: space-between;
  border-radius: 4px;

  &:focus {
    border-color: var(--primary500);
    outline: 1px solid var(--primary500);
  }

  &:hover {
    color: var(--primary500);
    border-color: var(--primary400);
  }

  &[aria-expanded="true"] {
    .icon-container {
      transform: rotateX(180deg);
    }
  }
}

.icon-container {
  height: 1.5rem;
  width: 1.5rem;
  margin-left: 0.25rem;
  transition: transform var(--animation-duration) var(--animation-function);

  @media (prefers-reduced-motion) {
    transition: none;
  }
}

.custom-select-field,
.option {
  display: flex;
  flex-flow: row nowrap;
  align-items: center;

  font-size: 1rem;

  height: var(--height);
  padding: 0 1rem;

  background-color: var(--gray50);
  transition: background-color var(--animation-duration)
    var(--animation-function);
  transition-property: border-color, background-color, outline;
  cursor: pointer;

  @media (prefers-reduced-motion) {
    transition: none;
  }

  span {
    user-select: none;
  }

  &:active {
    background-color: var(--primary200);
  }
}

.option {
  &[aria-selected="true"] {
    text-decoration: underline;
    color: var(--primary700);
    span {
      color: var(--primary700);
    }
  }

  &.highlighted {
    background-color: var(--primary100);
    &:active {
      background-color: var(--primary200);
    }
  }
}

.options {
  width: var(--width);
  margin: 0;
  padding: 0;
  border-radius: 4px;
  z-index: 10;
  border: 1px solid var(--gray100);
  min-height: var(--height);
  max-height: calc(var(--height) * 5);
  overflow-y: auto;
  overflow-x: hidden;
  box-shadow: 2px 2px 4px 0px rgba($color: #000000, $alpha: 0.2);
}
</style>
