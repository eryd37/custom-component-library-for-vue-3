<script setup lang="ts">
import type { Option } from "@/types";
import { v4 as uuidv4 } from "uuid";
import { computed, onBeforeMount, ref, watch } from "vue";
import Icon from "../decoration/Icon.vue";
import LabelText from "./LabelText.vue";
import SuggestionPopover from "./options/SuggestionPopover.vue";

const props = defineProps({
  type: {
    type: String as () =>
      | "text"
      | "number"
      | "password"
      | "email"
      | "search"
      | "tel"
      | "url",
    default: "text",
  },
  modelValue: [String, Number],
  id: {
    type: String,
    default: uuidv4(),
  },
  label: {
    type: String,
    default: "",
  },
  placeholder: String,
  variant: {
    type: String as () => "outlined" | "filled" | "underlined",
    default: () => "outlined",
  },
  required: Boolean,
  disabled: Boolean,
  compact: Boolean,
  fill: Boolean,
  inputWidth: {
    type: String,
    default: "initial",
  },
  maxLength: Number,
  minLength: Number,
  min: Number,
  max: Number,
  step: Number,
  integers: Boolean,
  icon: String,
  uppercase: Boolean,
  suggestions: Array as () => Option[],
});

const focus = ref(false);
const suggestionMenuOpen = ref(false);
const textValue = ref("");
const focusedSuggestionIndex = ref(0);

onBeforeMount(() => {
  textValue.value =
    typeof props.modelValue === "string" ? props.modelValue : "";
});

watch(props, (oldProps, newProps) => {
  if (typeof props.modelValue === "string") {
    textValue.value = newProps.modelValue as string;
  }
});

const emit = defineEmits(["update:modelValue"]);

const autocompletes = computed(() => {
  if (typeof textValue.value === "string" && props.suggestions) {
    return props.suggestions?.filter((suggestion) =>
      suggestion.key
        .toLowerCase()
        .includes(textValue.value.toString().toLowerCase())
    );
  }
  return [];
});

function handleChange(event: Event) {
  const target = event.target as HTMLInputElement;
  if (props.type === "number") {
    let number = Number(target.value);
    if (isNaN(number)) return;
    if (props.integers) {
      number = Math.round(number);
    }
    number = Math.min(number, props.max ?? Number.MAX_SAFE_INTEGER);
    number = Math.max(number, props.min ?? Number.MIN_SAFE_INTEGER);
    emit("update:modelValue", number);
  } else {
    emit(
      "update:modelValue",
      props.uppercase ? target.value.toUpperCase() : target.value
    );
  }
}

function handleInput(event: Event) {
  const target = event.target as HTMLInputElement;
  if (props.type === "text" && props.suggestions?.length) {
    textValue.value = props.uppercase
      ? target.value.toUpperCase()
      : target.value;
    suggestionMenuOpen.value = true;
    focusedSuggestionIndex.value = 0;
  }
}

function pickSuggestion(option: Option | undefined) {
  if (option) {
    emit("update:modelValue", option.name);
    suggestionMenuOpen.value = false;
  }
}

function handleKeyDown() {
  if (
    autocompletes.value.length &&
    focusedSuggestionIndex.value !== autocompletes.value.length - 1
  ) {
    focusedSuggestionIndex.value = focusedSuggestionIndex.value + 1;
  }
}

function handleKeyUp() {
  if (autocompletes.value.length && focusedSuggestionIndex.value !== 0) {
    focusedSuggestionIndex.value = focusedSuggestionIndex.value - 1;
  }
}

function handleEnter() {
  if (autocompletes.value.length) {
    pickSuggestion(autocompletes.value[focusedSuggestionIndex.value]);
    focusedSuggestionIndex.value = 0;
  }
}

function handleBeforeInput(event: Event) {
  const inputEvent = event as InputEvent;
  if (inputEvent.inputType === "insertLineBreak") {
    handleEnter();
  }
}

function handleFocus() {
  focus.value = true;
  suggestionMenuOpen.value = true;
}

function handleBlur() {
  focus.value = false;
  setTimeout(() => {
    suggestionMenuOpen.value = false;
  }, 100);
}
</script>

<template>
  <div class="input-field-wrapper" :class="{ compact, fill }">
    <label-text
      :label="label"
      :focus="focus"
      :disabled="disabled"
      :labelFor="id"
      :required="required"
    />
    <div
      class="input-wrapper"
      :class="{ [variant]: variant, compact, fill, disabled, focus }"
    >
      <input
        :type="type"
        :value="type === 'text' && suggestions?.length ? textValue : modelValue"
        @change="handleChange"
        @input="handleInput"
        @beforeinput="handleBeforeInput"
        @keyup.up="handleKeyUp"
        @keyup.down="handleKeyDown"
        @keyup.enter="handleEnter"
        @focus="handleFocus"
        @blur="handleBlur"
        :style="{ maxWidth: inputWidth }"
        :id="id"
        :name="label"
        :placeholder="placeholder"
        :disabled="disabled"
        :class="{ [variant]: variant, compact }"
        :maxlength="maxLength"
        :minlength="minLength"
        :max="max"
        :min="min"
        :step="step"
      />
      <icon v-if="icon" :name="icon" color="#94a3b8" size="small" />
    </div>
    <suggestion-popover
      :open="
        Boolean(
          suggestionMenuOpen &&
            textValue.length &&
            suggestions &&
            autocompletes?.length
        )
      "
      :options="autocompletes"
      @change="pickSuggestion"
      @close="suggestionMenuOpen = false"
      :focusedIndex="focusedSuggestionIndex"
    />
  </div>
</template>

<style lang="scss" scoped>
@import "@/assets/styles/mixins.scss";
.input-field-wrapper {
  @include flex-flow(column);
  align-items: flex-start;
  width: fit-content;
  position: relative;

  &.fill {
    width: 100%;
  }
  &.compact {
    width: fit-content;
  }

  &:hover > label {
    color: var(--primary400);
    &.disabled {
      color: var(--gray200);
    }
  }
}

.options {
  top: 5rem;
}

.input-wrapper {
  @include flex-flow(row);
  align-items: center;
  justify-items: center;
  width: 100%;
  height: 3rem;
  padding: 0.5rem;

  transition: border-color 150ms ease-in-out;
  transition-property: border-color background-color;
  border-bottom: 2px solid var(--gray200);
  &.outlined {
    background-color: var(--gray50);
    border: 1px solid var(--gray200);
    border-radius: 0.3rem;
  }
  &.underlined {
    padding: 0;
  }
  &.focus,
  &:hover {
    border-color: var(--primary400);
  }

  &.disabled {
    border-color: var(--gray100);
    background-color: transparent;
  }
  &.compact {
    width: fit-content;
    height: fit-content;
    padding: 0.1875rem 0.125rem;
  }
}

input {
  width: 100%;
  min-height: 100%;
  padding: 0;
  border: none;
  outline: none;
  background-color: transparent;
  caret-color: var(--primary400);

  font-size: 1rem;
}
</style>
