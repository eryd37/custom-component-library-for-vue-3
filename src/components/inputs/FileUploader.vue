<script setup lang="ts">
import { v4 as uuidv4 } from "uuid";
import { ref } from "vue";
import CustomButton from "../buttons/CustomButton.vue";
import LabelText from "./LabelText.vue";

const id = uuidv4();

const props = defineProps({
  modelValue: {
    type: Array as () => (File | null)[],
  },
  accept: String,
  fill: Boolean,
  label: String,
  dropAreaLabel: {
    type: String,
    default: "Drop file here",
  },
  browseLabel: {
    type: String,
    default: "Browse for file…",
  },
  wrongFileLabel: {
    type: String,
    default: "Wrong file type",
  },
  noFileChosenLabel: {
    type: String,
    default: "No file chosen",
  },
  filesChosenLabel: {
    type: String,
    default: "files chosen",
  },
});

const emit = defineEmits<{
  (e: "update:modelValue", files: (File | null)[] | undefined): void;
}>();

const wrongFileType = ref(false);

function areFilesValid(files: DataTransferItemList | FileList) {
  for (let i = 0; i < files.length; i++) {
    const file = files[i];
    if (props.accept === ".ics" && file.type !== "text/calendar") {
      wrongFileType.value = true;
      break;
    } else {
      wrongFileType.value = false;
    }
  }
}

function getFiles<T extends FileList | DataTransferItemList>(items: T) {
  const files: (File | null)[] = [];
  for (let i = 0; i < items.length; i++) {
    if ("getAsFile" in items[i]) {
      files.push((items[i] as DataTransferItem).getAsFile());
    } else {
      files.push(items[i] as File);
    }
  }
  return files;
}

const focusDragover = ref(false);

function handleFileDrop(event: DragEvent) {
  event.preventDefault();
  if (event.dataTransfer?.items.length) {
    const items = event.dataTransfer.items;
    const files = getFiles(items);
    if (files.length && !wrongFileType.value) {
      emit("update:modelValue", files);
    }
  }
  focusDragover.value = false;
  wrongFileType.value = false;
}

function handleDragenter(event: DragEvent) {
  focusDragover.value = true;
  if (event.dataTransfer?.items.length) {
    const files = event.dataTransfer.items;
    areFilesValid(files);
  }
}

function handleDragexit(event: DragEvent) {
  focusDragover.value = false;
  wrongFileType.value = false;
}

function handleFakeBrowseClick() {
  browseInput.value?.click();
}

async function handleFileBrowse(event: Event) {
  const target = event.target as HTMLInputElement;
  if (target.files?.length) {
    const file = getFiles(target.files);
    emit("update:modelValue", file);
  }
}

const browseFocused = ref(false);
const browseInput = ref<HTMLInputElement | undefined>(undefined);
</script>

<template>
  <div class="file-uploader-wrapper" :class="{ fill }">
    <label-text :label="label" :label-for="id" :focus="browseFocused" />
    <div class="file-drop-wrapper">
      <div
        class="drag-drop-file"
        :class="{ focus: focusDragover, error: wrongFileType }"
        @drop="handleFileDrop"
        @dragover="$event.preventDefault()"
        @dragenter="handleDragenter"
        @dragexit="handleDragexit"
      >
        <span
          v-for="(file, index) in modelValue"
          class="file-name"
          v-show="index < 4 || (modelValue && modelValue?.length <= 5)"
          :title="file?.name"
          >{{ file?.name }}</span
        >
        <span v-if="modelValue && modelValue?.length > 5">{{
          `+${modelValue.length - 4} flere filer`
        }}</span>
        <span v-if="!modelValue?.length">{{ dropAreaLabel }}</span>
      </div>
      <span v-if="wrongFileType" class="error-text">{{
        props.wrongFileLabel
      }}</span>
    </div>
    <div class="file-browser">
      <span>or</span>
      <input
        :id="id"
        type="file"
        ref="browseInput"
        multiple
        :accept="accept"
        @change="handleFileBrowse"
        @focus="browseFocused = true"
        @blur="browseFocused = false"
      />
      <div class="fake-browse-input">
        <div class="browse-button">
          <custom-button
            variant="outlined"
            @click="handleFakeBrowseClick"
            :label="browseLabel"
            :icon="{ name: 'upload' }"
          />
        </div>
        <div class="browse-file-name">
          <span
            v-if="modelValue?.length === 1"
            class="file-name"
            :title="modelValue[0]?.name"
            >{{ modelValue[0]?.name }}</span
          >
          <span v-else-if="modelValue && modelValue?.length > 1">{{
            `${modelValue?.length} ${filesChosenLabel}`
          }}</span>
          <span v-else>{{ noFileChosenLabel }}</span>
        </div>
      </div>
    </div>
  </div>
</template>

<style scoped lang="scss">
@import "@/assets/styles/mixins.scss";

.file-uploader-wrapper {
  @include flex-flow(column);
  row-gap: 0.5rem;
  width: fit-content;
  &.fill {
    width: 100%;
  }
  &:hover > label {
    color: var(--primary400);
  }
}

.file-drop-wrapper {
  width: 100%;
}

.error-text {
  color: var(--error900);
}
.drag-drop-file {
  @include flex-flow(column);

  height: 8rem;
  padding: 0.5rem;
  margin-bottom: 0.25rem;
  border: 1px solid var(--gray200);
  border-radius: 0.3rem;

  background-color: var(--gray50);
  transition: border-color 200ms ease-in-out;

  font-size: 1rem;
  &:hover,
  &.focus {
    border-color: var(--primary400);
  }
  &.error {
    border-color: var(--error900);
  }
  &:-moz-drag-over {
    border: 1px solid var(--primary500);
  }
}

.file-name {
  white-space: pre-wrap;
  text-overflow: ellipsis;
  overflow: hidden;
}

.file-browser {
  @include flex-flow(column);
  row-gap: 0.25rem;

  > input {
    visibility: hidden;
    display: none;
  }
  > .fake-browse-input {
    @include flex-flow(row);
    align-items: center;
    column-gap: 1rem;
    max-height: 4rem;
    overflow: hidden;
    border: 1px solid var(--gray200);
    border-radius: 0.3rem;
    padding: 0.5rem;
    font-size: 1rem;
    background-color: var(--gray50);
    transition: border-color 200ms ease-in-out;
    &:hover,
    &:focus {
      border-color: var(--primary400);
    }
    > div {
      width: fit-content;
    }
  }
}

.browse-button {
  min-width: 10.5rem;
}

.browse-file-name {
  text-overflow: ellipsis;
  overflow: hidden;
  max-height: 3rem;
}
</style>
