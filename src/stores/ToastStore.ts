import { defineStore } from "pinia";

export interface Toast {
  message: string;
  durationMs?: number;
  actionLabel?: string;
  actionCallback?: () => void;
}

export const useToastStore = defineStore("toast", {
  state: () => {
    return {
      toast: undefined as Toast | undefined,
      toastQueue: [] as Toast[],
    };
  },
  actions: {
    showToast(toast: Toast) {
      if (!this.toast) {
        this.toast = toast;
        this.handleDuration();
      } else {
        this.toastQueue.push(toast);
      }
    },
    pickNextToast() {
      if (this.toastQueue.length) {
        this.toast = undefined;
        setTimeout(() => {
          this.toast = this.toastQueue[0];
          this.toastQueue = this.toastQueue.slice(1);
          this.handleDuration();
        }, 150);
      } else {
        this.toast = undefined;
      }
    },
    handleDuration() {
      if (this.toast && (this.toast.durationMs || !this.toast.actionCallback)) {
        setTimeout(() => {
          this.pickNextToast();
        }, this.toast.durationMs ?? 3000);
      }
    },
  },
});
